# <center>Fundamentos de Robótica Móvil</center>

<center>Edición 2022</center>

Repositorio de la materia electiva Fundamentos de Robótica Móvil de la Carrera de Ingeniería Elctrónica de la Universidad Tecnólogica Nacional.

Página Oficial de la materia:

https://www.profesores.frc.utn.edu.ar/electronica/fundamentosroboticamovil/

En este repositorio econtraran materiales útiles de la materia.

### Directorios del repositorio:
 * [notebooks](notebooks/): encontraran las notebooks de los prácticos de la materia.



## Instalando los paquetes necesarios para correr Jupyter

Necesitamos los siguientes paquetes en python:

 * [Jupyter](https://docs.jupyter.org/en/latest/)
 * [numpy](https://numpy.org/doc/stable/reference/index.html)
 * [Pandas](https://pandas.pydata.org/docs/user_guide/index.html)
 * [MatPlotLib](https://matplotlib.org/stable/users/index)
 * [Seaborn](https://seaborn.pydata.org/api.html#)

```bash
~$: sudo apt update
~$: sudo apt install jupyter
~$: sudo apt install python3-seaborn python3-pandas python3-numpy 
```
Otra alternativa es anaconda o [miniconda](https://docs.conda.io/en/latest/miniconda.html)

### Correr jupyter

Moverse al directorio donde deseen crear las notebooks y ahi correr el siguiente comando:

```sh
~$: jupyter-notebook
```

